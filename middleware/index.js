function loggedOut(req, res, next){
    if (req.session && req.session.userId){
        return res.redirect('/profile');
    }
    return next();
}
module.exports.loggedOut = loggedOut;

function reqLogin(req, res, next) {
    if(req.session.userId && req.session){
        return next();
    } else {
        var err = new Error('Requires Login');
        err.status = 401;
        return next(err);
    }
}
module.exports.reqLogin = reqLogin;